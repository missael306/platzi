///////////////////////////////////////////////////////////////////////LISTAR
let tb = document.getElementById("tb");
get("assets/php/insert.php", "list").then(function(response) {
  let Jresponse = JSON.parse(response);

  Jresponse.forEach(function(item, index) {
    let tr = document.createElement("div");
    tr.className = "tr";

    let name = document.createElement("div");
    name.id = `name${item.idPersona}`;
    name.textContent = item.nombrePersona;

    let lastname = document.createElement("div");
    lastname.id = `lastname${item.idPersona}`;
    lastname.textContent = item.apellidosPersona;

    let options = document.createElement("div");

    let edit = document.createElement("p");
    edit.id = `edit${item.idPersona}`;
    edit.className = "edit";
    edit.textContent = "Editar";

    let sup = document.createElement("p");
    sup.id = `delete${item.idPersona}`;
    sup.className = "delete";
    sup.textContent = "Eliminar";

    options.appendChild(edit);
    options.appendChild(sup);

    tr.appendChild(name);
    tr.appendChild(lastname);
    tr.appendChild(options);

    tb.appendChild(tr);
  });
  editar();
  eliminar();
});

/////////////////////////////////////////////////////////////////////// EDITAR
//Obtiene todos  los botones para editar
function editar() {
  let editar = document.querySelectorAll(".edit");
  editar.forEach(function(item, index) {
    item.addEventListener("click", function(event) {
	  let id = event.target.id; // Identifica el id del elemento que lanzo el evento
	  id = id.slice(-2)
      let nombre = document.getElementById("name" + id).textContent;
      let apellido = document.getElementById("lastname" + id).textContent;
      document.getElementById("name").value = nombre;
      document.getElementById("last_name").value = apellido;
      document.getElementById("action").value = "update";
      document.getElementById("id").value = id;
    });
  });
}

//////////////////////////////////////////////////////////////////////////ELIMINAR
//obtiene todos los botones para eliminar
function eliminar() {
  let eliminar = document.querySelectorAll(".delete");

  eliminar.forEach(function(item, index) {
    item.addEventListener("click", function(event) {
      if (confirm("¿Deseas eliminar el registro?")) {
		let id = event.target.id; // Identifica el id del elemento que lanzo el evento
		id = id.slice(-2)
        get("assets/php/insert.php", "delete", id).then(function(response) {
          let Jresponse = JSON.parse(response);
          console.log(Jresponse.clave);
        });
      }
    });
  });
}

////////////////////////////////////////////////////////////////////////////INSERTAR
//btiene el boton tipo submit para hacer un insert
let submit = document.getElementById("sub");

submit.addEventListener("click", function(event) {
  event.preventDefault(); //previene su comportamiento por default para hacer la peticion get
  let id = document.getElementById("id").value;
  let action = document.getElementById("action").value;
  let name = document.getElementById("name").value;
  let last_name = document.getElementById("last_name").value;

  get("assets/php/insert.php", action, id, name, last_name)
    .then(function(response) {
      let Jresponse = JSON.parse(response);
      console.log(Jresponse.clave);
    })
    .catch(console.log);
});

///////////////////////////////////////////////////////////////////////////////FUNCIÓN XHR
function get(URL, action = "delete", id = 0, name = "", last_name = "") {
  //New Promise recibe una funcion
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
      const DONE = 4; //La solicitud se completo correctamente
      const OK = 200; //La solicitud tuvo exito
      if (this.readyState === DONE) {
        if (this.status === OK) {
          //Todo OK
          resolve(this.response);
        } else {
          //Hubo un error
          reject(
            new Error(
              `Se produjo un error al realizar el request ${this.status}`
            )
          );
        }
      }
    };
    xhr.open("POST", URL); //Tipo de peticion y ruta a la que se hace la petición
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); //Cabecera que permite el envio de parametros
    xhr.send(`action=${action}&id=${id}&name=${name}&last_name=${last_name}`); //Envio de parametros en post
  });
}
