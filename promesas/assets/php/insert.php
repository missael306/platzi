<?php
include_once 'conexion.php';
$nombre = $_POST["name"];
$apellidos = $_POST["last_name"];

switch ($_POST["action"]) {

    case "update":
        $id = $_POST["id"];
        $query = "UPDATE Persona SET nombrePersona='" . $nombre . "',apellidosPersona='" . $apellidos . "' WHERE idPersona = " . $id;
        $resultado = mysqli_query($conexion, $query) or die("Algo ha ido mal en la consulta a la base de datos");
        error($result);
        break;

    case "insert":
        $query = "INSERT INTO Persona( nombrePersona, apellidosPersona) VALUES ('" . $nombre . "','" . $apellidos . "') ";
        $resultado = mysqli_query($conexion, $query) or die("Algo ha ido mal en la consulta a la base de datos");
        error($result);
        break;

    case "delete":
        $id = $_POST["id"];
        $query = "DELETE FROM Persona WHERE idPersona = " . $id;
        $resultado = mysqli_query($conexion, $query) or die("Algo ha ido mal en la consulta a la base de datos");
        error($result);
        break;

    case "list":
        $query = "SELECT * FROM Persona";
        $resultado = mysqli_query($conexion, $query) or die("Algo ha ido mal en la consulta a la base de datos");
        $personas = array();
        $pos = 0 ;
        while ($persona = mysqli_fetch_array($resultado)) {
            $personas[$pos] = $persona;
            $pos++;
        }
        echo json_encode($personas);
        break;

    default:
        echo json_encode(array("clave" => "Accion indefinida"));
        break;

}

function error($err)
{
    $message = (!$err) ? "Accion completada" : "Intentarlo mas tarde";
    echo json_encode(array("clave" => $message));
}
