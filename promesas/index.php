<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JS - Platzi</title>
    <!-- style -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="content">
        <h1>Mi primer curso Js en Platzi</h1>
        
        <form action="assets/php/insert.php"  method="post">
            <label for="name">Nombre: </label>
            <input type="text" name="name" id="name">
            <label for="last_name">Apellidos: </label>
            <input type="text" name="last_name" id="last_name">
            <input type="hidden" name="action" id="action" value="insert">
            <input type="hidden" name="id" id="id" >
            <input type="submit" id = "sub" value="Enviar">
        </form>

        <form action="assets/php/conexion.php" method="post">
            <input type="hidden" name="action" value="list">
        </form>

        <div class="list">
            <div class="th">
                <div>Nombre</div>
                <div>Apellidos</div>
                <div>Acciones</div>
            </div>
            <div class="tb" id = "tb">
                <div class="edit"></div>
            </div>
        </div>
        
    </div>
    <script src="assets/js/index.js"></script>
</body>
</html>