/* Recursión sin memoizacion */

let iteraciones_de_la_funcion = 0;

function fibonacci(num) {
  if (num == 0) return 0;
  if (num == 1) return 1;
  iteraciones_de_la_funcion++;
  console.log(`Iteración de la función: ${iteraciones_de_la_funcion}`);
  return fibonacci(num - 1) + fibonacci(num - 2);
}
/* Representa el numero de veces que se ejecuta la función */

console.log(`Resultado ${fibonacci(20)}`);

/////////////////////////////////////////////////////////////////////////////

/* Solucion al problema memoizacion */

let iteraciones_de_la_funcion = 0;
function memo_fibonacci(num, memoria = {}) {
  if (memoria[num]) return memoria[num];
  if (num == 0) return 0;
  if (num == 1) return 1;
  iteraciones_de_la_funcion++;
  console.log(`Iteración de la función: ${iteraciones_de_la_funcion}`);
  return (memoria[num] =
    memo_fibonacci(num - 1, memoria) + memo_fibonacci(num - 2, memoria));
}
console.log(`Resultado ${memo_fibonacci(20)}`);

/* En la solucion anterior una vez que se ha calculado el fibonacci de 3  por ejemplo este resultado
se almacena en un array de forma que no se deba volver a re calcular  ahorrando tiempo de ejecucion y 
esfuerzo, optimizando la función */
