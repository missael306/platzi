
/* La serie fibonnacci representa una sucecion de numero comenzado por 0 y 1 como caso base 
(principio de la recursión) los numeros concecuentes son la suma de los dos anteriores, */

/* En la siguiente función, el número que recibe como parametro se parte hasta llegar a 1 o 0 
siendo estos el caso base, formando una especie de arbol binario, ejemplo en la imagen recursion.png */
function fibonacci(num) {
  if (num == 0) return 0
  if (num == 1) return 1

  return fibonacci(num - 1) + fibonacci(num - 2)
}

fibonacci(1)// RES: 1
fibonacci(2)// RES: 1
fibonacci(3)// RES: 2
fibonacci(4)// RES: 3
fibonacci(5)// RES: 5
fibonacci(6)// RES: 8
fibonacci(7)// RES: 13

/* Este tipo de recursión tiene un problema al calcular numeros grandes ya que en el caso planteado en la imagen
notese como la función calculara 2 veces el fibonacci de 3 y de 2 provacando mas carga de trabajo al equipo.
En el siguiente ejemplo Memoización se evita  ese problema guardando los resultados ya calculados */