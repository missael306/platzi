/* Generadores, clase definida dentro del lenguaje. Incluye  el metodo next que retorna el siguiente elemento 
y el metodo return que  puede finalizar el generador en su propiedad done,si se le pasa un parametro a un generador
ya terminado el atributo value tomara el valor del parametro  */

/*para realizar un generador son necesarios el * despues de function y la palabra yield que marca
el punto a partir de donde se volvera a ejecutar el código*/
function* fibonacci() {
  let a = 0,
    b = 1;

  while (true) {
    //al ser un ciclo infinito y con la palabra yield la ejecución se reanudad desde el ciclo sin pasar por la asignacion de las varibales
    let f = a;
    a = b;
    b = f + a;
    let reset = yield f;
    if (reset) (a = 0), (b = 1);//esto reinicia el calculo fibonacci a 1 y 0
  }
}

const fibo = fibonacci();
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next(true));
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.next());
console.log(fibo.return());
console.log(fibo.return(8));
