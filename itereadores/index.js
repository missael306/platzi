
/* los iteradores son una estructura de datos que permite almacenar listas de objetos o datos de forma 
infinita o finita a traves de su atributo done */

function fibonacci(){
    /* Esta funcion retorna un objeto con el valor del elemento actiual y la propiedad done 
    como false de forma que permite realizar listas infinitas */
    let a = 0, b = 1 ;
    return {
        next: ()=>{
            let f = a 
            a = b 
            b = f + a //las variables anteriores solo salvan los valores anteriores, b almacena el calculo del siguiente elemento
            return { value: f, done: false}
        }
    }
}

const fibo = fibonacci()
console.log(fibo.next().value)
console.log(fibo.next().value)
console.log(fibo.next().value)
console.log(fibo.next().value)
console.log(fibo.next().value)
console.log(fibo.next().value)
console.log(fibo.next().value)
console.log(fibo.next().value)